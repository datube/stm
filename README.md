# stm: Simple Task Manager

Python script which gives you the ability to create task at the commandline.
It will create tasks at the current folder, so you can have multiple task
"projects" and not just global tasks. When invoked without any arguments it
will show the task list; each task shows the remaining time in which it should
be completed. ou can add tasks and set the end date by several "human"
understandable time-stamping (see further on).


## installation

Just copy the script `stm` to a directory part of the PATH environment
variable to make it execute when typed at any location.

example: `cp stm /usr/local/bin/stm`


## usage

Get the list of available options:

```bash
stm --help
```

To get a list of tasks for the current "project", type

```bash
stm
```


### Add task
 
 * Navigate to the "project" folder
 * Add a new task which is due within 4 weeks at 3 PM

```bash
# example
stm -n 'task title' -d 4wT15:00
```


### Mark task as completed

 * Navigate to the "project" folder
 * Mark specific task as completed

```bash
# example
stm -c 1
```


### Remove task

  * list the current tasks
  * Remove a task

```bash
# example
stm -r 1
```


### Purge task

When a task are purged, the actual data is removed!

 * Navigate to the "project" folder
 * Purge the actual task data for a removed task

```bash
# example
stm -P 1
```
